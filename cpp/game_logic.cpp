#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "gameInit", &game_logic::on_game_init }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  const auto& gameTick = msg.get("gameTick", -1);

  std::cout << "game tick: " << gameTick << std::endl;

  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  for (size_t i = 0, len = data.size(); i < len; ++i)
  {
    if (data[i]["id"]["name"].as<std::string>() != "1DonutToRuleThemAll") { continue; }

    double  angle             = data[i]["angle"].as<double>();
    int     piece_index       = data[i]["piecePosition"]["pieceIndex"].as<int>();
    double  in_piece_distance = data[i]["piecePosition"]["inPieceDistance"].as<double>();

    std::cout << "Car Poision: " << "angle: " << angle
                                 << "piece index: " << piece_index
                                 << "in piece distance: " << in_piece_distance << std::endl;
  }
  
  return { make_throttle(0.5) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{

  return { make_ping() };
}