#include "piece.h"

piece::piece()
  : piece(0.0f, false, 0, 0.0f, piece::type::UNDEFINED) 
  {}

piece::piece(float length, bool lane_switch/* = false*/)
  : piece(length, lane_switch, 0, 0.0f, piece::type::STRAIGHT)
  {}

piece::piece(int radius, float angle)
  : piece(0.0f, false, radius, angle, piece::type::BEND)
  {}

piece::piece(float lenght, bool lane_switch, int radius, float angle, type piece_type)
{
  this->length       = length;
  this->lane_switch  = lane_switch;
  this->radius       = radius;
  this->angle        = angle;
  this->piece_type   = piece_type;
}