#ifndef HWO_PIECE_H
#define HWO_PIECE_H

class piece
{
public:
  enum class type
    {
      STRAIGHT,
      BEND,
      UNDEFINED
    };

  piece();
  piece(float length, bool lane_switch = false);
  piece(int radius, float angle);
  piece(float lenght, bool lane_switch, int radius, float angle, type piece_type);

  type get_piece_type() { return piece_type; }

  int  get_length() { return length;      }
  bool is_switch()  { return lane_switch; }

  int   get_radius() { return radius; }
  float get_angle()  { return angle;  }

private:
  float length;
  bool  lane_switch;
  int   radius;
  float angle;

  type piece_type;
};

#endif